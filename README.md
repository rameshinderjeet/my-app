# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
This project is available on bitbucket publically. Use git clone to clone the project and then go to my-app folder to install package to start the app.

### git clone https://Rameshinderjeet@bitbucket.org/rameshinderjeet/my-app.git
### cd my-app
# Use Yarn to run this project as it creates two separate lock files if we use npm and yarn.

## Available Scripts

In the project directory, you can run:

### `yarn install`

Install all the node module dependencies to run this project.

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3006](http://localhost:3006) to view it in the browser.

The json file is added in public folder and will be fetched by using localhost:3006 as base url.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn test -- --coverage`

To get the test coverage report, use the above mentioned command.

 <br /> <br /> <br /> <br />


### ADDED THE MASTER BRANCH