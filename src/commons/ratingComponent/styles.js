import styled from 'styled-components';
import styleVars from '../styles-global/global';

export const RatingContainer = styled.div`
    margin-left: ${styleVars.dimensions.$spacing4};
    color: lightgrey;
    display: flex;
    .checked {
        color: rgb(251, 203, 59);
    }
`