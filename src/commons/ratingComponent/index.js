import React, { useState, useEffect } from 'react';
import { RatingContainer } from './styles';

const  RatingComponent = (props) => {
    const { 
        rating,
        ratingType,
    } = props;
    const [halfRating, setHalfRating] = useState(0);
    const [fullRating, setFullRating] = useState(0);
    const [selfRating, setSelfRating] = useState(false);
    
    useEffect(() => {
        if (rating % 1 !== 0) {
            setHalfRating(1);
        }
        setFullRating(parseInt(rating));
    }, [rating]);

    useEffect(() => {
        if (ratingType === 'self') {
            setSelfRating(true);
        }
    }, [ratingType]);
    
    return (
        <RatingContainer>
            {[...Array(fullRating)].map((item,index) => {   
                return (         
                    <span key={index} className={selfRating ? "fa fa-circle checked" : "fa fa-star checked"}></span>  
                );
            })}
            {[...Array(halfRating)].map((item,index) => {
                return (         
                    <span key={index} className={selfRating ? "fa-solid fa-circle-half checked" : "fa fa-star-half checked"} ></span>  
                );
            })}
            {[...Array(5-fullRating-halfRating)].map((item,index) => {        
                return (         
                    <span key={index} className={selfRating ? "fa fa-circle" : "fa fa-star"}></span>  
                );
            })}
        </RatingContainer>
    );
};

export default RatingComponent;