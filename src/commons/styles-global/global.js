const styleVars = {
  color: {
    // Color theme
    'text_color': '#148CC0',
    'background_color': '#f7f7f7',

    // Colors
    'white': '#FFFFFF',
    'white_hover': '#ESF4FA',

  },

  fonts: {
    'fontSemiboldWt': '600',
    'fontLightWt': '300',
    'fontRegularWt': '400'
  },

  dimensions: {
    '$spacing0': '0px',
    '$spacing4': '4px',
    '$spacing8': '8px',
    '$spacing12': '12px',
    '$spacing16': '16px',
    '$spacing24': '24px',
    '$spacing32': '32px',
    '$spacing36': '36px',
    '$spacing48': '48px',
    '$spacing64': '64px',
    '$spacing72': '72px'
  },
  breakpoints: {
    'desktop_extra_large': '(min-width: 1281px)',
    'desktop_large': '(min-width: 961px) and (max-width: 1280px)',
    'desktop': '(min-width: 769px) and (max-width: 960px)',
    'tablet': '(min-width: 481px) and (max-width: 768px)',
    'tabletPortrait': '(min-width: 769px)',
    'mobile': '(max-width: 480px)',
    'mobile_small': '(max-width: 320px)'
  }
}

export {
  styleVars as default
}
