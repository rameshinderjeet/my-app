import styled from 'styled-components';
import styleVars from '../styles-global/global';

export const H1 = styled.h1`
  line-height: 32px;
  font-size: 32px;
  margin: ${styleVars.dimensions.$spacing0};
`

export const H2 = styled.h2`
  line-height: 24px;
  font-size: 20px;
  margin: ${styleVars.dimensions.$spacing0};
`

export const H3 = styled.h3`
  line-height: 20px;
  font-size: 16px;
  margin: ${styleVars.dimensions.$spacing0};
`
export const P = styled.p`
  line-height: 16px;
  font-size: 14px;
  margin: ${styleVars.dimensions.$spacing0};
`
