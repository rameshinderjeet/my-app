import React from 'react'
import PropTypes from 'prop-types'

import { H1, H2, H3, P } from './styles'

const Heading = (props) => {
  switch (props.level) {
    case '1':
      return (
        <H1 {...props}>{props.children}</H1>
      )
    case '2':
      return (
        <H2 {...props}>{props.children}</H2>
      )
    case '3':
      return (
        <H3 {...props}>{props.children}</H3>
      )
    case '4':
      return (
        <P {...props}>{props.children}</P>
      )
    default:
      return (
        <H1 {...props}>{props.children}</H1>
      )
  }
}

Heading.propTypes = {
  level: PropTypes.string

}

export {H1, H2, H3, P}

export default Heading
