import React from 'react';
import renderer from 'react-test-renderer';
import {
  render,
  cleanup,
} from '@testing-library/react'
// import userEvent from '@testing-library/user-event'
import { Provider } from 'mobx-react';
import { makeObservable, observable,} from 'mobx';
import App from '../App';
import {
  /* Hotel Related Actions */
  getHotelsList
} from '../store/models/hotel/actions';

afterEach(cleanup);

class HotelStore {
  hotelsList = [];
  constructor () {
    makeObservable(this, {
        hotelsList: observable,
    });
  };
  getHotelsList = getHotelsList;
}

it('renders correctly', () => {
    const hotelStore = new HotelStore();
    const tree = renderer
      .create(    
        <Provider hotelStore = {hotelStore}>
            <App />
        </Provider>)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });