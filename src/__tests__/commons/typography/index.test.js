import { render, screen } from '@testing-library/react';
import Heading, {H1, H2, H3, P} from '../../../commons/typography';

test('renders h1 tag', () => {
  render(<H1>Heading H1</H1>);
  const linkElement = screen.getByText('Heading H1');
  expect(linkElement).toBeInTheDocument();
});

// describe('Heading', () => {
//     it('renders the correct heading', () => {
//         const { getByText } = render(<Heading {...props} />);
//         getByText(props.level);
//     });
// });

test('renders h1 using level', () => {
    render(<Heading level='1'>Heading H1</Heading>);
    const linkElement = screen.getByText('Heading H1');
    expect(linkElement).toBeInTheDocument();
  });

  test('renders h2 using level', () => {
    render(<Heading level='2'>Heading H2</Heading>);
    const linkElement = screen.getByText('Heading H2');
    expect(linkElement).toBeInTheDocument();
  });

  test('renders h3 using level', () => {
    render(<Heading level='3'>Heading H3</Heading>);
    const linkElement = screen.getByText('Heading H3');
    expect(linkElement).toBeInTheDocument();
  });

  test('renders p using level', () => {
    render(<Heading level='4'>Paragraph</Heading>);
    const linkElement = screen.getByText('Paragraph');
    expect(linkElement).toBeInTheDocument();
  });

  test('renders default heading', () => {
    render(<Heading>Default</Heading>);
    const linkElement = screen.getByText('Default');
    expect(linkElement).toBeInTheDocument();
  });