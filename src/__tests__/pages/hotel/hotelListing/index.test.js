import React from 'react';
import { render, screen, cleanup, waitFor } from '@testing-library/react';
// import { renderHook, act } from "@testing-library/react-hooks";
// import useHotelListing from '../../../../pages/hotel/hotelListing/useHotelListing';
import HotelList from '../../../../pages/hotel/hotelListing';

afterEach(cleanup);

const fakeHotelList = [{
    id: "cxd650nuyo",
    property: {
      propertyId: "P107801",
      title: "Courtyard by Marriott Sydney-North Ryde",
      address: [
        "7-11 Talavera Rd",
        "North Ryde"
      ],
      previewImage: {
        url: "https://unsplash.it/145/125/?random",
        caption: "Image of Courtyard by Marriott Sydney-North Ryde",
        imageType: "PRIMARY"
      },
      rating: {
        ratingValue: 4.5,
        ratingType: "self"
      }
    },
    offer: {
      promotion: {
        title: "Exclusive Deal",
        type: "MEMBER"
      },
      name: "Deluxe Balcony Room",
      displayPrice: {
        amount: 329,
        currency: "AUD"
      },
      savings: {
        amount: 30,
        currency: "AUD"
      },
      cancellationOption: {
        cancellationType: "NOT_REFUNDABLE"
      }
    }
  }]; 

    test('should render hotelName', async () => {
        render(<HotelList  hotelList={fakeHotelList[0]} key={fakeHotelList[0].id}/>)
        const hotelName = await waitFor(() => screen.getByText('Deluxe Balcony Room'));
        expect(hotelName).toBeInTheDocument();
    })