import React from 'react'
import {
  render,
  cleanup,
} from '@testing-library/react'
// import userEvent from '@testing-library/user-event'
import { Provider } from 'mobx-react';
import { makeObservable, observable,} from 'mobx';
import Hotel from '../../../pages/hotel';
import {
  /* Hotel Related Actions */
  getHotelsList
} from '../../../store/models/hotel/actions';

// import {server} from 'test/server'
// beforeAll(() => server.listen())
// afterAll(() => server.close())
// afterEach(() => server.resetHandlers())

afterEach(cleanup);

class HotelStore {
  hotelsList = [];
  constructor () {
    makeObservable(this, {
        hotelsList: observable,
    });
  };
  getHotelsList = getHotelsList;
}

const renderWithStore = hotelStore => 
  render (
    <Provider hotelStore = {hotelStore}>
      <Hotel />
    </Provider>
  )

it('renders hotel list', () => {
  const hotelStore = new HotelStore();
  const { getByText } = renderWithStore(hotelStore);
  const linkElement = getByText('0 hotels in');
  expect(linkElement).toBeInTheDocument();
})

// test('should render hotels', () => {
//   render(<Hotel />);
//   const linkElement = screen.getByText('hotels in Sydney');
//   expect(linkElement).toBeInTheDocument();
// })