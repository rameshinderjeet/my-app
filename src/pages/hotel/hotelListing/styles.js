import styled from 'styled-components';
import styleVars from '../../../commons/styles-global/global';
import { H1, H2, H3, P } from '../../../commons/typography';

export const ListingContainer = styled.div`
    display: flex;
    flex: 1 1 auto;
    -webkit-box-pack: justify;
    justify-content: space-between;
    flex-direction: row;
`

export const ImageBlock = styled.div`
    flex-basis: 10.125rem;
    padding: ${styleVars.dimensions.$spacing12};
`

export const DescriptionBlock = styled.div`
    display: flex;
    flex: 1 1 0px;
    flex-direction: row;
    border-top: 1px solid lightGrey;
    margin-left: ${styleVars.dimensions.$spacing4};
`

export const HotelDetails = styled.div`
    flex-basis: 250px;
    padding: ${styleVars.dimensions.$spacing12};
`

export const PriceDetails = styled.div`
    padding: ${styleVars.dimensions.$spacing48} 0 0 0;
    display: flex;
    flex: 1 1 auto;
    align-items: flex-end;
    flex-direction: column;
`

export const NameContainer = styled.div`
    display: flex;
    width: 80%;
`

export const HotelName = styled.a`
    cursor: pointer;
    text-decoration: none;
    display: inline-block;
    color: #000000;
`

export const HotelNameText = styled(H3)`
    width: 150px;
    white-space: nowrap;
    overflow: hidden !important;
    text-overflow: ellipsis;
`

export const RoomLink = styled.a`
    cursor: pointer;
    text-decoration: underline;
    display: inline-block;
    color: red;
    padding: ${styleVars.dimensions.$spacing24} 0px;
`

export const Address = styled(P)`
    color: grey;
`

export const CancelText = styled(P)`
    color: green;
`

export const PriceValue = styled(H1)`
    padding: ${styleVars.dimensions.$spacing16} 0px;
`

export const Discount = styled(H2)`
    color: red;
`

export const Rating = styled.div`
    color: rgb(251, 203, 59);
    margin-left: ${styleVars.dimensions.$spacing4};
    display: flex;
`

export const CircleRating = styled.div`
    height: 15px;
    width: 15px;
    background-color: rgb(251, 203, 59);
    border-radius: 50%;
    display: inline-block;
`