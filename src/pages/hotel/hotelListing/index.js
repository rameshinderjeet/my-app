import React from 'react';
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
import { P } from '../../../commons/typography';
import useHotelListing from './useHotelListing';
import RatingComponent from '../../../commons/ratingComponent';
import { 
    ListingContainer, 
    ImageBlock, 
    DescriptionBlock, 
    HotelDetails, 
    PriceDetails, 
    NameContainer,
    HotelName,
    RoomLink,
    CancelText,
    Address,
    PriceValue,
    Discount,
    HotelNameText,
} from './styles';

function HotelListing (props) {

    const { 
        hotelList,
    } = props;

    const {
        hotelListingData,
    } = useHotelListing({
        hotelList,
      });
    const { 
        imgSrc,
        hotelName,
        hotelAddress,
        roomType,
        cancellationText,
        hotelPrice,
        discount,
        rating,
        ratingType,
    } = hotelListingData;

    // console.log('hotelName in app>>', hotelName);
    return (
        <ListingContainer>
            <ImageBlock>
                { imgSrc !== '' ?
                    <img src={imgSrc} alt={'hotel'} /> :
                    <Skeleton width={'100%'} height={'100%'}/>
                }
            </ImageBlock>
            <DescriptionBlock>
                <HotelDetails>
                    <NameContainer>
                        { hotelName !== '' ?
                            <HotelName aria-label={hotelName} data-expanded-clickable-area-target="true" rel="noreferrer noopener" href="" target="_self">
                                <HotelNameText data-testid='hotel-name'>
                                    {hotelName} 
                                </HotelNameText>
                            </HotelName>
                            :
                            <Skeleton width={'200px'} />
                        }
                        {rating !== '' && <RatingComponent rating={rating} ratingType={ratingType} />}
                    </NameContainer>
                    <Address>
                        {hotelAddress !== '' ?
                            hotelAddress :
                            <Skeleton width={'100%'} />
                        }
                    </Address>
                    <RoomLink>{roomType}</RoomLink>
                    <CancelText>{cancellationText}</CancelText>
                </HotelDetails>
                <PriceDetails>
                    <P>1 night total (AUD)</P>
                    <PriceValue>${hotelPrice}</PriceValue>
                    { discount > 0 ?
                        <Discount>Save ${discount}</Discount>:
                        null
                    }
                </PriceDetails>
            </DescriptionBlock>
        </ListingContainer>
    )
}

export default HotelListing;