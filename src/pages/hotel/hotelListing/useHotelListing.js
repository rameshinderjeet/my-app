import { useEffect, useState } from 'react';

function useHotelListing (props) {
    const {
        hotelList,
    } = props;

    const [hotelListingData, setHotelListingData] = useState({
        imgSrc: '',
        hotelName: '',
        hotelAddress: '',
        roomType: '',
        cancellationText: '',
        hotelPrice: '',
        discount: '',
        rating: '',
        ratingType: '',
    });

    useEffect(() => {
        let hotelAdr = '';
        // console.log('hotelList in hook', hotelList?.offer?.name);
        if (hotelList && hotelList.property) {
            hotelAdr = hotelList.property.address[0] +', '+ hotelList.property.address[1];
        }
        setTimeout(() => {
            setHotelListingData({
                imgSrc: hotelList?.property.previewImage?.url,
                hotelName: hotelList?.property?.title,
                hotelAddress: hotelAdr,
                roomType: hotelList?.offer?.name,
                cancellationText: hotelList?.offer?.cancellationOption?.cancellationType,
                hotelPrice: hotelList?.offer?.displayPrice?.amount,
                discount: hotelList?.offer?.savings?.amount,
                rating: hotelList?.property?.rating?.ratingValue,
                ratingType: hotelList?.property?.rating?.ratingType,
            });

        },0);
        // console.log('hotelListingData in hook', hotelListingData);
    }, [hotelList]);

    return {
        hotelListingData,
    };
}

export default useHotelListing;