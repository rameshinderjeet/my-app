import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { useHotelStore } from '../../store/models';
import Logo from '../../commons/images/brandImage/logo.png';
import { H3 } from '../../commons/typography';
import HotelList from './hotelListing';

//styling using styled component
import { Container, LogoContainer, Content, SortText } from './styles';

function Hotel () {
    const hotelStore = useHotelStore();
    const [hotelList, setHotelList] = useState([]);
    useEffect(() => {
        /* eslint-disable react-hooks/exhaustive-deps */
        hotelStore.getHotelsList();
    }, []);

    useEffect(() => {
        setHotelList(hotelStore.hotelsList);
    }, [hotelStore.hotelsList]);

    return (
        <Container>
            <LogoContainer>
                <img src={Logo} alt="Logo" />
            </LogoContainer>
            <Content>
                <H3>
                    {hotelList.length} hotels in <b>Sydney</b>
                </H3>
                <SortText><H3><b>Sort by</b></H3></SortText>
            </Content>
            {
                hotelList.length > 0 ? 
                    hotelList.map( (list) => <HotelList hotelList={list} key={list.id} /> ) : null
            }
            
        </Container>
    )
}

export default observer(Hotel);