import styled from 'styled-components';
import styleVars from '../../commons/styles-global/global';

export const Container = styled.div`
    display: block;
    width: 80%;
    margin: 0 auto;
`

export const LogoContainer = styled.div`
    padding: ${styleVars.dimensions.$spacing24};
    padding-left: 0;
    height:50px;
    width: 80px;
    display: flex;
`

export const Content = styled.div`
    display: flex;
    justify-content: space-between;
    margin: ${styleVars.dimensions.$spacing12} 0;
`

export const SortText = styled.div`
    
`