import axios from 'axios';
const EndpointSources = {
    remote: () => ({
        getHotels: 'http://localhost:3006/hotelsData.json',
    }),

    local: () => ({
        getHotels: 'http://localhost:3006/hotelsData.json',
    }),
};

// base url can be defined and automatically changed based on environment variables

// const baseURL = {
//     /* LOCAL doesn't need base URL because every go service is on port */
//     local: '',
//     prod: '',
//     stage: '',
//     dev: '',
// };

// function getBaseURL () {
//     if (baseURL[window.env]) {
//         return baseURL[window.env];
//     }
//     return baseURL.dev;
// }

export function endpoints () {
    if (window.env !== undefined && window.env !== 'local') {
       return EndpointSources.remote();
    }
    return EndpointSources.local();
}

export function get (endpoint, options = { traceId: '', headers: {} }) {
    const {
        headers,
        traceId,
    } = options;

    async function execRequest (resolve, reject) {
        const reqHeaders = {
            'x-trace-id': traceId,
            ...headers,

        };

        axios
        .get(
            endpoint,
            { headers: reqHeaders }
        )
        .then(function (response) {
            // if (dispatchErrors({
            //     action: window.envStore.errorStore.action,
            //     store: window.envStore.errorStore,
            //     response,
            // })) {
            //     return;
            // }
            resolve(response);
        })
        .catch(function (error) {
            // dispatchErrors({
            //     action: window.envStore.errorStore.action,
            //     store: window.envStore.errorStore,
            //     response: error.response,
            // });
            // resolve(error.response);
        });
    }
    return new Promise(execRequest);
};