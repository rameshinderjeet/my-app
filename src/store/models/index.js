import { v4 as uuidv4 } from 'uuid';
import HotelStore from './hotel';

export { useHotelStore } from './hotel';

const uniqueTraceId = uuidv4();

const hotelStore = new HotelStore();

/* Change this to Point to Environment Variable */
window.env = process.env.REACT_APP_ENV_VAR;
// window.env = 'dev';
// window.env = 'remote';
// window.env = 'local';
hotelStore.Environment = window.env;
hotelStore.traceId = uniqueTraceId;

const models = {
    hotelStore,
};

export default models;

export const stores = {
    hotelStore,
};

hotelStore.stores = stores;
