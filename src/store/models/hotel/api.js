import {
    get,
    endpoints,
} from '../../api';

export function apiGetHotels (options) {
    return get(
        endpoints().getHotels,
        options
    );
}