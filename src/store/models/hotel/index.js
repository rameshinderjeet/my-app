import { makeObservable, observable } from 'mobx';
import { useStores } from '../useStores';
import {
    /* Hotel Related Actions */
    getHotelsList
} from './actions';

export default class HotelsStore {
    version = {}
    stores = {}
    env = null
    traceId = null
    hotelsList = []
    assetsLoading = false

    observed = {
        importedEvents: null,
        assets: {},
    };

    constructor () {
        makeObservable(this, {
            hotelsList: observable,
            assetsLoading: observable,
        });
    }

    /* Hotel Related Actions */
    getHotelsList = getHotelsList;
}

export function useHotelStore () {
    const { hotelStore } = useStores();
    return hotelStore;
}
