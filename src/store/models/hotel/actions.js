import {
    apiGetHotels,
} from './api';

export async function getHotelsList () {
    this.assetsLoading = true;
    const res = await apiGetHotels({
        traceId: 'asdjhhkasd-asd21-asdasd12-fasda',
    });

    if (res.data && res.data.results) {
        console.log('response.data', res.data.results);
        this.hotelsList = res.data.results;
        this.assetsLoading = false;
        // setTimeout(() => {
        //     this.hotelsList = res.data.results;
        //     this.assetsLoading = true;
        // }, 5000);
    } else {
        console.log('response.error', res);
    }
}
