import models from './models';
const store = {
    ...models,
};

export default store;
