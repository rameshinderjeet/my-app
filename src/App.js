import React from 'react';
import {
  BrowserRouter,
  Route,
  Routes,
} from 'react-router-dom';

import Hotel from './pages/hotel';

function App () {

  return (
    <React.Fragment>
       <BrowserRouter>
          <Routes>
            <Route exact path="/" element={<Hotel />} />
            {/* <Route component={Error404} /> */}
          </Routes>
        </BrowserRouter>

    </React.Fragment>
  );
}

export default App;
